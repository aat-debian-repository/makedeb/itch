# Maintainer: Antoni Aloy Torrens <aaloytorrens@gmail.com>

# From Arch Linux AUR:
# Contributor: Łukasz Mariański <lmarianski at protonmail dot com>
pkgname=itch
pkgver=25.6.2
pkgrel=0
_commit="209ecbadc4a2675b61ee6d9daeab029dd94bedfe"
pkgdesc="The best way to play your itch.io games"
arch=('amd64')
url="https://github.com/itchio/itch"
license=('MIT')
depends=('libgtk-3-0' 'libnotify4' 'libxss1' 'libnss3' 'desktop-file-utils' 'hicolor-icon-theme')
makedepends=('itch-setup' 'unzip')
install='itch.install'
# Forgot to tag 25.6.2 release in Github src
#"$pkgname-$pkgver-src.tar.gz::https://github.com/itchio/itch/archive/refs/tags/v$pkgver.tar.gz"

# Disable itch.patch for now
# "itch.patch"
# '8262cbfc13289dd49f944466d33f83b860cfe7a2610d78b2c44c0b51abd30fc4'
source=(
	"$pkgname-$pkgver-src.tar.gz::https://gitea.antonialoytorrens.com/unaffiliated-mirrored-sources/itch-tmp/archive/v$pkgver.tar.gz"
	"io.itch.itch.desktop"
)
sha256sums=('884d1cda481514a52736a4d92de6ad92fc6717dbfdd343f21505a266134f7d1a'
            'fd7a7aed47f3e3d9fe4f834492030faf9401f3df0ff35289589f07716b42bdd6')
prepare() {
	# Default locale
	export LANG="en_US.UTF-8"

	itch-setup --silent
}

package() {
	mkdir -p "$pkgdir"/opt/itch
	cp -ar $HOME/.itch/app-$pkgver/* "$pkgdir"/opt/itch
	rm -rf $HOME/.itch

	# Now patch files
	#cd "$pkgdir"/opt/itch
	#patch -p1 < "$srcdir"/itch.patch

	# Copy LICENSES
	cd "$pkgdir"/opt/itch
	install -Dm644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname/"
	install -Dm644 LICENSES.chromium.html -t "$pkgdir/usr/share/licenses/$pkgname/"

	# Icons
	#cd "$srcdir"/$pkgname-$pkgver
	
	# Temporary location
	cd "$srcdir"/$pkgname-tmp
	for icon in release/images/itch-icons/icon*.png
	do
		iconsize="${icon#release/images/itch-icons/icon}"
		iconsize="${iconsize%.png}"
		icondir="${pkgdir}/usr/share/icons/hicolor/${iconsize}x${iconsize}/apps/"
		install -d "${icondir}"
		install -Dm644 "$icon" "$icondir/$pkgname.png"
	done

	# Application file
	install -Dm644 "$srcdir/io.itch.$pkgname.desktop" -t "${pkgdir}/usr/share/applications/"	
}
